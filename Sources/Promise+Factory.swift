//
//  Promise+Factory.swift
//  Promises
//
//  Created by Nikolay Petrov on 9/30/15.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation

extension Promise {
    public static func make<Value>() -> DeferredPromise<Value> {
        return DeferredPromise<Value>()
    }

    public static func make<Value>(with value: Value) -> Promise<Value> {
        let promise: DeferredPromise<Value> = make()
        promise.fulfill(value)
        return promise
    }

    public static func make<Value>(withError error: Error) -> Promise<Value> {
        let promise: DeferredPromise<Value> = make()
        promise.reject(error)
        return promise
    }

    public static func make<Value>(on queue: DispatchQueue = DispatchQueue.main, block: @escaping (DeferredPromise<Value>) throws -> Void) -> Promise<Value> {
        let promise: DeferredPromise<Value> = make()
        queue.async {
            do {
                try block(promise)
            } catch {
                promise.reject(error)
            }
        }
        return promise
    }

    public static func make<Value>(on queue: DispatchQueue = DispatchQueue.main, block: @escaping () throws -> Value) -> Promise<Value> {
        let promise: DeferredPromise<Value> = make()
        queue.async {
            do {
                promise.fulfill(try block())
            } catch {
                promise.reject(error)
            }
        }
        return promise
    }

    public static func make<Value>(on queue: OperationQueue, block: @escaping (DeferredPromise<Value>) throws -> Void) -> Promise<Value> {
        let promise: DeferredPromise<Value> = make()
        queue.addOperation {
            do {
                try block(promise)
            } catch {
                promise.reject(error)
            }
        }
        return promise
    }

    public static func makeAfter(_ interval: TimeInterval) -> Promise<Void> {
        let promise: DeferredPromise<Void> = make()
        promise.fulfill(())
        return promise.delay(interval)
    }
}
